/* RFID reader access control
configuration of pins in files myLCD_config.h and spi_config.h
make sure LCD data pins are all in the same port
*/
#define F_CPU 20000000UL
#include <avr/io.h>
#include <util/delay.h>
#include "spi.h"
#include "mfrc522.h"
#include "myLCD.h"
#include "myRFID.h"

#define PORT_LED	PORTC
#define DDR_LED		DDRC
#define PIN_LED		PINC
#define GREEN_LED   PC1
#define RED_LED		PC2

void alarm(void);

uint8_t SelfTestBuffer[64];

 
int main()
{
	//init LEDS and BUZZER
	PORT_LED &= ~(0x01 << GREEN_LED) & ~(0x01 << RED_LED);
	DDR_LED |= (0x01 << GREEN_LED) | (0x01 << RED_LED);
	PORTC &= ~0x08;
	DDRC |= 0x08;
	
	uint8_t byte;
	uint8_t str[MAX_LEN];
	initialize_lcd();
	spi_init();
	_delay_ms(50);
	
	RFID_init();
	_delay_ms(50);
	set_cursor(line1);
	send_string("RFID Reader:");
	set_cursor(line2);
	
	//check version of the reader
	byte = mfrc522_read(VersionReg);
	if(byte == 0x92) send_string("MIFARE RC522v2");
	else if(byte == 0x91 || byte==0x90)	send_string("MIFARE RC522v1");
	else send_string("No reader found");
	
	_delay_ms(2000);
	clear_lcd();
	uint8_t * serial_out;
	*serial_out = 0;
	
	
	while(1){
		//byte = mfrc522_request(PICC_REQALL,str);
// 		
// 		_delay_ms(1000);
// 		clear_lcd();
		byte = mfrc522_get_card_serial(serial_out);
		
		
		
	} 
}

void alarm(void) {
	for(int i=0; i<5; i++) {
		PORTC |= 0x08;
		_delay_ms(500);
		PORTC &= ~0x08;
		_delay_ms(500);
	}
}
