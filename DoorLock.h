#define PORT_LED	PORTC
#define DDR_LED		DDRC
#define PIN_LED		PINC
#define GREEN_LED   PC4
#define RED_LED		PC5
#define PORT_BUZ    PORTC
#define DDR_BUZ     DDRC
#define BUZ         PC3
#define PORT_SERVO  PORTD
#define DDR_SERVO   DDRD
#define PIN_SERVO   PIND
#define SERVO       PD3
#define PIN_JOYSTICK PIND
#define DDR_JOYSTICK DDRD
#define JOYSTICK_KEY PD2
#define JOYSTICKX    PC1
#define JOYSTICKY    PC2

#define GREEN_ON    PORT_LED |= 0x01 << GREEN_LED;
#define GREEN_OFF   PORT_LED &= ~(0X01 << GREEN_LED);
#define RED_ON      PORT_LED |= 0x01 << RED_LED;
#define RED_OFF     PORT_LED &= ~(0X01 << RED_LED);
#define BUZ_ON      PORT_BUZ |= (0x01 << BUZ);
#define BUZ_OFF     PORT_BUZ &= ~(0x01 << BUZ);
#define SERVO_CLOSE  PORT_SERVO  &= ~(0x01 << SERVO);
#define SERVO_OPEN PORT_SERVO |= 0x01 << SERVO;


void led_buz_button_servo_init(void);
void alarm(void);
void check_reader_version(void);
int compare_UID(uint8_t UID1[], uint8_t UID2[]);
void print_UID(uint8_t UID[]);
void change_authorization(uint8_t UID[]);
void ADC_init(void);
uint16_t ADC_read(uint8_t channel);
int insert_code(char * code);
int check_joystick(char direction, uint16_t joystickX, uint16_t joystickY);