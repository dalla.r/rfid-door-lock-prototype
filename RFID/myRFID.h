#include "mfrc522_reg.h"
#include <stdint.h>

#define CARD_FOUND		1
#define CARD_NOT_FOUND	2
#define ERROR			3


void RFID_init(void);
void RFID_write(uint8_t reg, uint8_t data);
uint8_t RFID_read(uint8_t reg);
void RFID_reset(void);
uint8_t	RFID_detect(uint8_t req_mode, uint8_t * tag_type);
uint8_t RFID_to_card(uint8_t cmd, uint8_t *send_data, uint8_t send_data_len, uint8_t *back_data);
uint8_t RFID_get_card_serial(uint8_t * serial_out);