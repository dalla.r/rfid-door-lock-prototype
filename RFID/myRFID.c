#include "myRFID.h"
#include "spi.h"
#include "myLCD.h"
#define F_CPU 16000000UL
#include "util/delay.h"

void RFID_init(void) {
	uint8_t byte;
	RFID_reset();
	
	RFID_write(TModeReg, 0x8D); //sets timer and upper 4 bits of prescaler
	RFID_write(TPrescalerReg, 0x3E); //lower 8 bits of prescaler (prescaler = 3390)
	RFID_write(TReloadReg_1, 0x1E); //upper 8 bits of timer reload value
	RFID_write(TReloadReg_2, 0x00); //lower 8 bits of timer reload value (timer reload value = 7680)
	RFID_write(TxASKReg, 0x40); //controls settings of transmission mode: sets 100% ASK (amplitude-shift keying) modulation
	RFID_write(ModeReg, 0x3D); //defines general modes for transmitting and receiving: transmitter only starts if RF field is generated
	
	byte = RFID_read(TxControlReg); //controls logical behaviour of antenna
	if(!(byte & 0x03)) {
		RFID_write(TxControlReg, byte | 0x03); //makes sure output signal on Tx1 and Tx2 deliver 13.56MHz energy carrier
	}
}

void RFID_write(uint8_t reg, uint8_t data) {
	ENABLE_CHIP(); //enables rfid chip setting SS to 0 (spi.h)
	spi_transmit((reg << 1) & 0x7E); //sends the address of the register we want to write to (Address Format: 0XXXXXX0)
	spi_transmit(data); //sends the data to write
	DISABLE_CHIP(); //disables rfid chip setting SS to 1 (spi.h)
}

uint8_t RFID_read(uint8_t reg) {
	uint8_t data;
	ENABLE_CHIP();
	spi_transmit(((reg << 1) & 0x7E) | 0x80); //sends the address of the register we want to read from (Address Format: 1XXXXXX0)
	data = spi_transmit(0x00); //reads the data from the buffer
	DISABLE_CHIP();
	return data;
}

void RFID_reset(void) {
	RFID_write(CommandReg, SoftReset_CMD); //write to the command register (0x01) the reset command 
										   //(0x0F, last 4 bits are the command bits)
}

uint8_t	RFID_detect(uint8_t req_mode, uint8_t * tag_type)
{
	uint8_t  status;

	RFID_write(BitFramingReg, 0x07); //used for transmission of bit oriented frames: defines the
								   	//number of bits of the last byte that will be transmitted
									//000b indicates that all bits of the last byte will be transmitted
	
	//these two lines basically just write to FIFOdatareg 
	tag_type[0] = req_mode;
	status = RFID_to_card(Transceive_CMD, tag_type, 1, tag_type);

	if (status != CARD_FOUND)
	{
		status = ERROR;
	}
	
	return status;
}

uint8_t RFID_to_card(uint8_t cmd, uint8_t *send_data, uint8_t send_data_len, uint8_t *back_data)
{
	uint8_t status = ERROR;
	uint8_t n;
	uint8_t	tmp;
	uint32_t i;

	n=RFID_read(ComIrqReg);
	RFID_write(ComIrqReg,n&(~0x80));//clear all interrupt bits
	n=RFID_read(FIFOLevelReg);
	RFID_write(FIFOLevelReg,n|0x80);//flush FIFO data
	
	RFID_write(CommandReg, Idle_CMD);	//Cancel the current cmd
	
	//Writing data to the FIFO
	for (i = 0; i < send_data_len; i++) {
		RFID_write(FIFODataReg, send_data[i]);
	}

	RFID_write(CommandReg, cmd); //Execute the cmd
	if (cmd == Transceive_CMD) {//this command transmits data from FIFO buffer to antenna and automatically activates the receiver after transmission								
		n=RFID_read(BitFramingReg);
		RFID_write(BitFramingReg,n|0x80); //bit 7 starts transmission of data
	}
	
	//Waiting to receive data to complete
	i = 2000;
	do {
		n = RFID_read(ComIrqReg);
		i--;
	}
	while ((i!=0) && !(n&0x01) && !(n&0x30)); //reads from ComIrqReg until Error bit is set or when the timer decrements the timervalue register to 0

    tmp=RFID_read(BitFramingReg);
    RFID_write(BitFramingReg,tmp&(~0x80)); //stop transmission of data
	
	if (i != 0)
	{
		if(!(RFID_read(ErrorReg) & 0x1B))	//if bits BufferOvfl Collerr CRCErr ProtecolErr are 0, basically checks if there were no errors
		{
			status = CARD_FOUND;
			if (n & 0x01) {//checks if the timer reached 0
				status = CARD_NOT_FOUND;
			}

			if (cmd == Transceive_CMD)
			{
				n = RFID_read(FIFOLevelReg); //number of bytes stored in FIFO (up to 64)
				
				for (i=0; i<n; i++) { //Reading the received data in FIFO
					back_data[i] = RFID_read(FIFODataReg);
				}
			}
		}
		else
		{
			status = ERROR;
		}
	}
	return status;
}


uint8_t RFID_get_card_serial(uint8_t * serial_out)
{
	uint8_t status;
	uint8_t i;
	uint8_t serNumCheck=0;
	
	RFID_write(BitFramingReg, 0x00);
	
	serial_out[0] = PICC_ANTICOLL;
	serial_out[1] = 0x20;
	status = RFID_to_card(Transceive_CMD, serial_out, 2, serial_out);

	if (status == CARD_FOUND)
	{
		//Check card serial number, I don't get it, it checks if the last byte is different than all the bytes EXORed together???
		for (i=0; i<4; i++)
		{
			serNumCheck ^= serial_out[i];
		}
		if (serNumCheck != serial_out[i])
		{
			status = ERROR;
		}
	}
	return status;
}
