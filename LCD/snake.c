#include "snake.h"
#include "myLCD.h"
#define F_CPU 16000000UL
#include <util/delay.h>

void snake() {
	char direction = 'R';
	clear_lcd();
	send_character(0xFF);
	send_instruction(shiftRight_lcd);
	while((PIN_JOYSTICK >> JOYSTICK_KEY) & 0x01) {
		uint16_t joystickX = ADC_read(JOYSTICKX);
		uint16_t joystickY = ADC_read(JOYSTICKY);
		if(joystickX < 324) direction = 'R'; 
		else if(joystickX > 700) direction = 'L';
		else if(joystickY < 324) direction = 'U';
		else if(joystickY > 700) direction = 'D';
		if(direction == 'R') {
			send_instruction(shiftRight_lcd);
			_delay_ms(300);
		}
		if(direction == 'L') {
			send_instruction(shiftLeft_lcd);
			_delay_ms(500);
		}
	}
}