#define F_CPU 16000000UL
#include "myLCD.h"
#include <util/delay.h>
#include <string.h>

//if changing pins in myLCD_config.h, functions send_instruction, send_character and initialize_lcd need to be adapted

void enable(void) { //LCD enable to have the instruction executed
	LCD_INST_PORT |= (0x01 << LCD_E);
	_delay_us(10);
	LCD_INST_PORT &= ~(0x01 << LCD_E);
	_delay_us(10);
	return;
}

void register_select(char string[])	{ //select if we want to send data (RS=1) or an instruction (RS=0)
	if(!(strcmp(string, "data"))) LCD_INST_PORT |= (0x01 << LCD_RS);
	else if(!(strcmp(string, "instruction"))) LCD_INST_PORT &= ~(0x01 << LCD_RS);
	_delay_us(5);
	return;
}

void send_instruction(uint8_t byte)	{ //send instruction as two separate nibbles: first clear upper nibble of LCD_DATA_PORT then set it to the desired value leaving the lower nibble unchanged
	register_select("instruction");
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | (byte & 0xF0);  
	enable();
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | ((byte << 4) & 0xF0);
	enable();
	return;
}

void send_character(uint8_t byte) { //send character as two separate nibbles: first clear upper nibble of LCD_DATA_PORT then set it to the desired value leaving the lower nibble unchanged
	register_select("data");
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | (byte & 0xF0);
	enable();
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | ((byte << 4) & 0xF0);
	enable();	
	return;
}

void send_string(char string[]) { //send string as a sequence of characters
	int i = 0;
	while (string[i] != '\0') {
		send_character(string[i]);
		i++;
		_delay_us(10);
	}
	return;
}

void set_cursor(int position) { //set cursor to any desired position 0xXY where X is row (0 for first and 4 is second row) and Y is column (0 to F)
	send_instruction(0x80 | position);
	_delay_us(10);
}

void initialize_lcd(void) {
	LCD_DATA_PORT &= ~(0xF0);
	LCD_DATA_DDR |= 0xF0;
	LCD_INST_PORT &= ~(0x01 << LCD_E) & ~(0x01 << LCD_RS);
	LCD_INST_DDR |= (0x01 << LCD_E) | (0x01 << LCD_RS);
	_delay_ms(100);
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | (reset_lcd & 0xF0); //not using send_instruction because the LCD is still in 8 bit mode
	enable();
	_delay_ms(20);
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | (reset_lcd & 0xF0); //in the reset_lcd instruction only the upper nibble matters
	enable();
	_delay_ms(20);
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | (reset_lcd & 0xF0); //so we send that nibble through our 4 pins
	enable();
	_delay_ms(20);
	LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | (functionSet_lcd_4bit & 0xF0); //in the functionSet_lcd_4bit instruction the bit controlling the 4 or 8 bit mode is in the upper nibble as well
	enable();
	_delay_us(80);
	send_instruction(functionSet_lcd_4bit); //now we have set 4 bit input and we can use send_instruction, we send functionSet_lcd_4bit again to set to 2 line display, 5x8 digits (lower nibble)
	_delay_us(80);
	send_instruction(displayOff_lcd);
	_delay_us(80);
	send_instruction(clear_display);
	_delay_ms(4);
	send_instruction(displayOn_lcd);
	_delay_us(80);
	return;
}

void clear_lcd(void) {
	send_instruction(clear_display);
	_delay_ms(2);
}

void scan_card(void) {
	clear_lcd();
	set_cursor(0x05);
	send_string("Please            Press key");
	set_cursor(0x41);
	send_string("scan your card      to change card");
}

void access_granted(void) {
	clear_lcd();
	send_string(" Access Granted");
}

void send_byte(uint8_t byte) { //function used as debugging tool, useful to print UID of the RFID cards
	int check_byte[8];
	for(int i = 0; i < 8; i++) 
		check_byte[i] = ((byte >> i)  & 0x01); //copy byte values as elements of "check_byte" array

	for(int i = 7; i >= 0; i--) { //print check_byte's elements from MSB to LSB
	if(check_byte[i]) send_character('1');
	else send_character('0');
	_delay_ms(1);
	}
}