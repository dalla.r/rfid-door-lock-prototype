#include <avr/io.h>

//config LCD pins
#define LCD_DATA_DDR		DDRD
#define LCD_DATA_PORT	    PORTD
#define LCD_DATA_PIN		PIND
#define LCD_INST_DDR     	DDRB
#define LCD_INST_PORT  	    PORTB
#define LCD_INST_PIN     	PINB
#define LCD_E		        PB1
#define LCD_RS           	PB0