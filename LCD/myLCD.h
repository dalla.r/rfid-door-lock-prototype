#include "myLCD_config.h"

// LCD instructions
#define clear_display            0b00000001          // clear display
#define home_lcd                 0b00000010          // return cursor to first digit on first line
#define entryMode_lcd            0b00000110          // shift cursor from left to right
#define displayOff_lcd           0b00001000          // turn display off
#define displayOn_lcd            0b00001100          // turn display on, cursor off, blink off
#define functionSet_lcd_8bit     0b00111000          // set the LCD to 8 bit input, 2 line display, 5x8 digits
#define functionSet_lcd_4bit     0b00101000          // set the LCD to 4 bit input, 2 line display, 5x8 digits
#define shiftRight_lcd           0b00011100          // shift display right
#define shiftLeft_lcd            0b00011000          // shift display left
#define space_lcd                0b00010100          // space
#define reset_lcd                0b00110000          // reset
#define line1                    0b00000000          // line 1
#define line2                    0b01000000          // line 2


void enable(void);
void register_select(char string[]);
void send_instruction(uint8_t byte);
void send_character(uint8_t byte);
void send_string(char string[]);
void set_cursor(int position);
void initialize_lcd(void);
void clear_lcd(void);
void scan_card(void);
void access_denied(void);
void access_granted(void);
void send_byte(uint8_t byte);