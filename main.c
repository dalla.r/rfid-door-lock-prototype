/* RFID reader access control
configuration of pins in files myLCD_config.h and spi_config.h and main.c
make sure LCD data pins are all in the same port
*/
#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include "spi.h"
#include "myLCD.h"
#include "myRFID.h"
#include "DoorLock.h"
 
int main()
{
	uint8_t authorized_UID[4] = {0xD2, 0x9B, 0x69, 0x10}; //UID of the RFID card (UID = D29B6910), if want to use RFID tag (UID = 515260D3D3)
	char code[] = {'U', 'D', 'L', 'L', 'R', 'D'}; //joystick code to change authorized card (Up, Down, Left, Right), changeable in length with no issues
	uint8_t byte;
	uint8_t UID_search[4];
	uint16_t joystickX = 0;
	
	led_buz_button_servo_init(); //initialize LEDs, buzzer and servo ports
	
	initialize_lcd(); //initialize LCD
	
	spi_init(); //initialize SPI communication
	_delay_ms(50);
	
	RFID_init(); //initialize RFID reader
	_delay_ms(50);
	
	ADC_init(); //initialize ADC
	
	set_cursor(line1);
	send_string("RFID controlled");
	set_cursor(0x43);
	send_string("door lock");
	_delay_ms(3000);
	
	scan_card(); //print "Please scan your card" to LCD (find it in myLCD.c)

	while(1) {
		joystickX = ADC_read(JOYSTICKX); //continuously assign ADC value on PC1 to joystickX
		
		//use joystick to shift LCD left or right
		if(joystickX>700) send_instruction(shiftLeft_lcd);
		else if (joystickX<400) send_instruction(shiftRight_lcd);

		RFID_detect(PICC_REQALL, UID_search); //detecting the RFID cards in front of the antenna

		byte = RFID_get_card_serial(UID_search); //reading card UID from FIFO data register
			
		if(byte == CARD_FOUND) {
			if(compare_UID(UID_search, authorized_UID)) { //comparing the read UID with the stored UID
					GREEN_ON;
					BUZ_ON;
					_delay_ms(800);
					BUZ_OFF;
 				access_granted();
				
				if(!(PORT_SERVO & (0x01<<SERVO))) { //if SERVO is 0, set it to 1
 					SERVO_OPEN; 
					set_cursor(0x43);
					send_string("*door open*");
				}
				else { //if SERVO is 1, set it to 0
					SERVO_CLOSE;
					set_cursor(0x42);
					send_string("*door closed*");
				}
 				_delay_ms(2000);
				GREEN_OFF;
			}
			else {
					RED_ON;
					access_denied();
					RED_OFF;
			}
			scan_card();
		}
		
		if (!((PIN_JOYSTICK >> JOYSTICK_KEY) & 0x01)) { //if JOYSTICK_KEY is pressed (0)
			while(insert_code(code)) { //insert_code returns 0 if the code is wrong, returns 1 if it's right
			    change_authorization(authorized_UID); //change authorized card
				break;
			}
			clear_lcd();
			scan_card();
			_delay_ms(200);
		} 
	}
}



void led_buz_button_servo_init(void) { //assigning all ports and setting input/outputs
	PORT_LED &= ~(0x01 << GREEN_LED) & ~(0x01 << RED_LED);
	DDR_LED |= (0x01 << GREEN_LED) | (0x01 << RED_LED);
	PORT_BUZ &= ~(0x01 << BUZ);
	DDR_BUZ |= 0x01 << BUZ;
	PORT_SERVO &= ~(0x01 << SERVO);
	DDR_SERVO |= 0x01 << SERVO;
	DDR_JOYSTICK &= ~(0x01 << JOYSTICK_KEY);
}

void access_denied(void) {
	 for(int i = 0; i < 4; i++) {
		 clear_lcd();
		 _delay_ms(300);
		 BUZ_ON;
		 send_string("Access Denied!!!");
		 _delay_ms(300);
		 BUZ_OFF;
	 }
}

void check_reader_version(void) {
	uint8_t byte;
	byte = RFID_read(VersionReg);
	set_cursor(line1);
	send_string("RFID Reader:");
	set_cursor(line2);
	if(byte == 0x92) send_string("MIFARE RC522v2");
	else if(byte == 0x91 || byte==0x90)	send_string("MIFARE RC522v1");
	else send_string("No reader found");
	_delay_ms(2000);
	clear_lcd();
}

int compare_UID(uint8_t UID1[], uint8_t UID2[]) {
	int result = 0;
	for(int i = 0; i < 4; i++) {
		if (UID1[i] == UID2[i]) result++;
	}
	if (result == 4) return 1;
	else return 0;
}

void print_UID(uint8_t UID[]) {
	int i = 0;
	set_cursor(line1);
	for(i = 0; i < 4; i++) {
		if(i==2) set_cursor(line2);
		send_byte(UID[i]);
	}
}

void change_authorization(uint8_t UID[]) { //overwrites the stored UID with the read one
	uint8_t new_UID[4];
	uint8_t UID_request[4];
	uint8_t search = 0;
	
	GREEN_ON;
	RED_ON;
	clear_lcd();
	set_cursor(0x05);
	send_string("Change");
	set_cursor(0x42);
	send_string("authorization");
	
	//wait for a card to be detected
	while(search!=CARD_FOUND) {
		RFID_detect(PICC_REQALL, UID_request); 
		search = RFID_get_card_serial(new_UID); //store the UID of the card
		if (!((PIN_JOYSTICK >> JOYSTICK_KEY) & 0x01)) { //if JOYSTICK_KEY is pressed (0) exit the function
			GREEN_OFF;
			RED_OFF;
			return;
		}
	}
	GREEN_OFF;
	RED_OFF;
	
	//if a card is detected
		if(search == CARD_FOUND) {
			int cmp = compare_UID(new_UID, UID); //compare the UID of the scanned card with the authorized UID
			if(cmp) {
				clear_lcd();
				set_cursor(0x02);
				send_string("Card already");
				set_cursor(0x43);
				send_string("authorized");
				_delay_ms(1000);
				return;
			}
			else if (!cmp) {
				clear_lcd();
				set_cursor(0x01);
				send_string("Authorizing...");
				set_cursor(0x40);

				for(int j = 0; j < 16; j++) { //display (fake) progress bar and blink LED green and red
					send_character(0xFF);
					RED_ON;
					GREEN_OFF;
					_delay_ms(150);
					GREEN_ON;
					RED_OFF;
					_delay_ms(150);
				}
				for(int i = 0; i < 4; i++) { //change authorized UID
					UID[i] = new_UID[i];
				}
				
				clear_lcd();
				set_cursor(0x04);
				send_string("Success!");
				_delay_ms(2000);
				GREEN_OFF;
				return;
			 }
		return;
	}
}

void ADC_init(void) {
	ADMUX = (1<<REFS0); //voltage reference selection
		
	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); //setting ADC enable bit and the prescaler bits (111 = 128) --> 16000000/128 = 125000
}

uint16_t ADC_read(uint8_t channel) {
	channel &= 0x07; //selecting the ADC channel 0-7 and ANDing with 7 to make sure 'channel� is kept between 0 and 7
	ADMUX = (ADMUX & 0xF8) | channel; //setting the desired channel after clearing the first 3 bits of ADMUX
	
	ADCSRA |= (1<<ADSC); //start conversion
	while(ADCSRA & (1<<ADSC)); //wait for conversion to complete, ADSC clears when complete
	
	return ADC; //return ADC conversion result, 10 bit long, two registers: ADCH and ADCL
}

int insert_code(char * code) { //read joystick code
	uint16_t joystickX = 0;
	uint16_t joystickY = 0;
	int check = -1;
	int k = 0;
	int dim = sizeof(code);
	clear_lcd();
	set_cursor(0x02);
	send_string("Insert code");
	set_cursor(0x45);
	_delay_ms(200);
	
	for(k = 0; k < dim; k++) { //repeat for each element of array code[]
		check = -1;
		//check direction of joystick movement
		while(check == -1) {
			joystickX = ADC_read(JOYSTICKX);
			joystickY = ADC_read(JOYSTICKY);
			check = check_joystick(code[k], joystickX, joystickY); //check if the movement of the joystick is the one needed for code[k]
			if(check == 1) send_character('*'); //if code[k] is entered correctly
			if(!check) { //if code[k] is wrong
				clear_lcd();
				set_cursor(0x03);
				send_string("Wrong code");
				_delay_ms(1000);
				return 0;
			}
			if (!((PIN_JOYSTICK >> JOYSTICK_KEY) & 0x01)) return 0; //if JOYSTICK_KEY is pressed (0) exit insert_code function
		}
		//wait for the joystick to go back to back to center
		while(!(joystickX < 700 && joystickX > 324 && joystickY < 700 && joystickY > 324)) {
			joystickX = ADC_read(JOYSTICKX);
			joystickY = ADC_read(JOYSTICKY);
		}
	}
	clear_lcd();
	set_cursor(0x01);
	send_string("Exact code!!!");
	_delay_ms(2000);
	return 1;
}

int check_joystick(char direction, uint16_t joystickX, uint16_t joystickY) { //check if the movement of the joystick is right according to the direction provided as parameter
	switch(direction) {
		case 'D':
			if(joystickY < 324) return 1;
			else if((joystickY > 700) | (joystickX > 800) | (joystickX < 224)) return 0;
		case 'U':
			if(joystickY > 700) return 1;
			else if((joystickY < 324) | (joystickX > 800) | (joystickX < 224)) return 0;
		case 'L':
			if(joystickX > 700) return 1;
			else if((joystickY < 224) | (joystickY > 800) | (joystickX < 324)) return 0;
		case 'R':
			if(joystickX < 324) return 1;
			else if((joystickY < 224) | (joystickY > 800) | (joystickX > 700)) return 0;
		default: return -1;
	}
}
