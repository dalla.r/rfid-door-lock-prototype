#include <avr/io.h>

#define SPI_CONFIG_AS_MASTER 	1

//config SPI pins

#define SPI_DDR		DDRB
#define SPI_PORT	PORTB
#define SPI_PIN		PINB
#define SPI_MOSI	PB3
#define SPI_MISO	PB4
#define SPI_SS		PB2
#define SPI_SCK		PB5
