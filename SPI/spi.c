#include "spi.h"

#if SPI_CONFIG_AS_MASTER
void spi_init()
{
	SPI_DDR |= (1<<SPI_MOSI)|(1<<SPI_SCK)|(1<<SPI_SS); //setting SPI outputs, I had to change to = to |= which caused big problems at the beginning
	SPCR |= (1<<SPE)|(1<<MSTR)|(1<<SPR0); //prescaler 16
}


uint8_t spi_transmit(uint8_t data)
{
	SPDR = data;					//SPDR: Writing to the register initiates data transmission
	while(!(SPSR & (1<<SPIF)));		//When a serial transfer is complete, the SPIF Flag (7) in SPSR is set (exit the loop)
	
	return SPDR;					//Reading the register causes the Shift register receive buffer to be read
}

#else
void spi_init()
{
	SPI_RS_PORT &= ~(0x01 << SPI_RS);		
	SPI_RS_DDR |= (0x01 << SPI_RS);
	SPI_DDR |= (1<<SPI_MISO);
	SPCR |= (1<<SPE);
}

uint8_t spi_transmit(uint8_t data)
{
	while(!(SPSR & (1<<SPIF)));
	return SPDR;
}
#endif
